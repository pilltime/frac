#[derive(Clone, Copy, Default)]
pub struct Image {
	pub(crate) width: u32,
	pub(crate) height: u32,
	pub(crate) zoom: f64,
}

impl Image {
	pub fn new(width: u32, height: u32, zoom: f64) -> Image {
		Image { width, height, zoom }
	}

	pub fn set_width(&mut self, new_width: u32) {
		self.width = new_width;
	}

	pub fn set_height(&mut self, new_height: u32) {
		self.height = new_height;
	}

	pub fn set_zoom(&mut self, new_zoom: f64) {
		self.zoom = new_zoom;
	}

	pub fn get_width(&self) -> u32 {
		self.width
	}

	pub fn get_height(&self) -> u32 {
		self.height
	}

	pub fn get_zoom(&self) -> f64 {
		self.zoom
	}
}
