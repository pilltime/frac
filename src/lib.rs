/* enum worth? prob not
pub enum Set {
	Mandelbrot,
//	Julia, | TODO (possibly...)
}
*/

pub mod fractal;
pub mod image;

use std::thread;

use fractal::Fractal;
use image::Image;

// TODO?: any way of making the parameters non static references?
pub fn calculate_mandelbrot(image: Image, fractal: Fractal, n_threads: u32) -> Vec<u8> {
	// Since the image will always be 8bit RGB, we can calculate the size of `data`
	let mut data = Vec::with_capacity((image.width * image.height * 3) as usize);

	let section = image.height / n_threads;
	let mut results = Vec::with_capacity(n_threads as usize);

	for i in 0..n_threads {
		results.push(thread::spawn(move || {
			calculate_mandelbrot_section(i * section, (i + 1) * section, image, fractal)
		}));
	}

	for _ in 0..n_threads {
		data.append(&mut results.remove(0).join().unwrap());
	}

	data
}

fn calculate_mandelbrot_section(from: u32, to: u32, image: Image, fractal: Fractal) -> Vec<u8> {
	let mut data = Vec::with_capacity((image.width * (to - from) * 3) as usize);

	for y in from..to {
		for x in 0..image.width {
			let (x0, y0) = calculate_c(x, y, &image, &fractal);
			let (mut x1, mut y1) = (0.0, 0.0);
			let (mut x2, mut y2) = (0.0, 0.0);

			// Shamelessly taken from wikipedia:
			// https://en.wikipedia.org/wiki/Plotting_algorithms_for_the_Mandelbrot_set#Optimized_escape_time_algorithms
			// I almost halves the time it takes. holy shit
			let mut iter = 0;
			while x2 + y2 <= 4.0 && iter < fractal.max_iter {
				y1 = 2.0 * x1 * y1 + y0;
				x1 = x2 - y2 + x0;
				x2 = x1.powi(2);
				y2 = y1.powi(2);
				iter += 1;
			}

			// Color scheme
			let (r, g, b) = if iter == fractal.max_iter {
				(0, 0, 0)
			} else {
				(
					((iter as f64 / 24.0).sin() * 127.0 + 128.0) as u8,
					((iter as f64 / 24.0 + 2.0).sin() * 127.0 + 128.0) as u8,
					((iter as f64 / 24.0 - 2.0).sin() * 127.0 + 128.0) as u8,
				)
			};

			data.push(r);
			data.push(g);
			data.push(b);
		}
	}

	data
}

// At zoom=1.0, 1000px == 1 unit in the complex space
fn calculate_c(x: u32, y: u32, image: &Image, fractal: &Fractal) -> (f64, f64) {
	// Amount of units that each side is
	let width_units = image.width as f64 / 1000f64 / image.zoom;
	let height_units = image.height as f64 / 1000f64 / image.zoom;

	// Amount of units to shift the image so that the chosen point is in the middle of the image
	let width_offset = width_units / 2f64;
	let height_offset = height_units / 2f64;

	(
		x as f64 / image.width as f64 * width_units + fractal.re - width_offset,
		y as f64 / image.height as f64 * height_units - fractal.im - height_offset,
	)
}
