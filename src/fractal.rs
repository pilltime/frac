#[derive(Clone, Copy, Default)]
pub struct Fractal {
	pub(crate) re: f64,
	pub(crate) im: f64,
	pub(crate) max_iter: u32,
}

impl Fractal {
	pub fn new(re: f64, im: f64, max_iter: u32) -> Fractal {
		Fractal { re, im, max_iter }
	}

	pub fn set_center(&mut self, new_re: f64, new_im: f64) {
		self.re = new_re;
		self.im = new_im;
	}

	pub fn set_max_iter(&mut self, new_max_iter: u32) {
		self.max_iter = new_max_iter;
	}

	pub fn get_center(&self) -> (f64, f64) {
		(self.re, self.im)
	}

	pub fn get_max_iter(&self) -> u32 {
		self.max_iter
	}
}
