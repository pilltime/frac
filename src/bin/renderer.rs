use std::fs::OpenOptions;
use std::io::BufWriter;

use png::{BitDepth, ColorType, Encoder};

use frac::calculate_mandelbrot;
use frac::fractal::Fractal;
use frac::image::Image;

fn main() {
	let file = OpenOptions::new()
		.write(true)
		.create(true)
		.open("./out/out.png")
		.unwrap();

	let image: Image = Image::new(3000, 2000, 1800000.0);
	let fractal: Fractal = Fractal::new(0.437924257, 0.341892075, 1000);

	let buffer = BufWriter::new(file);
	let mut encoder = Encoder::new(buffer, image.get_width(), image.get_height());
	encoder.set_color(ColorType::RGB);
	encoder.set_depth(BitDepth::Eight);
	let mut writer = encoder.write_header().unwrap();

	let data = calculate_mandelbrot(image, fractal, 20);
	writer.write_image_data(&data).unwrap();
}
